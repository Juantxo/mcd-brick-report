this.MCD["init_globals"] = this.MCD["init_globals"] || {};

this.MCD["init_globals"] = () => {
  // ******************************************  //
  // ************** GLOBALS  MCD ********************  //
  // ******************************************  //

  // Wrapping the STUDY object
  this.MCD["study"] = this.MCD["study"]
    ? this.MCD["study"]
    : this.MCD.wrappers.dataFilters.filterStudy();

  // Wrapping the chartData object
  this.MCD["chartData"] = this.MCD["chartData"] ? this.MCD["chartData"] : {};

  return false;
};
