this.MCD = this.MCD || {};

this.d3.json("./assets/data/bricks.json").then(data => {
  this.MCD.init(data);
  return false;
});

this.MCD.init = data => {
  console.log("this.MCD.init", data);

  // Wrapping the data object
  this.MCD["database"] = this.MCD["database"] ? this.MCD["database"] : data;
  this.MCD.init_globals();
  this.MCD.init_data();
  this.MCD.init_draw();

  console.log("this.MCD.window", window.MCD);
  return false;
};
