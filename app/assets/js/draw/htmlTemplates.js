this.MCD = window["MCD"] || {};
this.MCD["draw"] = window["MCD"]["draw"] || {};

/// **********************************************************************************
// draw html Template Variables
// **********************************************************************************

this.MCD.draw["htmlTemplates"] = () => {
  let d = this.MCD.study;
  let tBody = this.d3.select("body");
  let templates = [
    "state",
    "id_study",
    "name",
    "address",
    "day",
    "hour",
    "kind_building"
  ];

  d.forEach(function(element) {
    if (templates.indexOf(element.key) > -1) {
      tBody.selectAll(".template-" + element.key).text((d, i) => {
        return element.value;
      });
    }
  });
};
