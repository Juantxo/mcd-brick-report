this.MCD = window["MCD"] || {};
this.MCD["draw"] = window["MCD"]["draw"] || {};

// **********************************************************************************
// draw PageNumber
// **********************************************************************************

this.MCD.draw.pageNumber = () => {
  let vPageNumber = this.d3.select("body").selectAll(".page_number");
  // RANDOM PHOTO POPULATION

  // Nümero de página
  let pageNumber = () =>
    vPageNumber.text((d, i) => {
      return i + 1;
    });

  return pageNumber();
};
