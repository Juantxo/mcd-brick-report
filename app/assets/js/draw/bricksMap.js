this.MCD = window["MCD"] || {};
this.MCD["draw"] = window["MCD"]["draw"] || {};
this.MCD["draw"]["bricksMap"] = window["MCD"]["draw"]["bricksMap"] || {};

this.MCD.draw.bricksMap = d => {
  let access_token = d.token;
  let latitude = d.d.y;
  let longitude = d.d.x;

  let marker_icon = this.L.icon({
    iconUrl: "./assets/images/" + d.marker,
    //shadowUrl: 'leaf-shadow.png',

    iconSize: d.iconSize // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    //iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    //shadowAnchor: [4, 62],  // the same for the shadow
    //popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
  });

  let locator_map = new this.L.map(d.chartElement, {
    zoomControl: d.zoomControl
  }).setView([latitude, longitude], d.zoom);

  this.L.control
    .zoom({
      position: d.zoomPosition
    })
    .addTo(locator_map);

  this.L.tileLayer(access_token, {
    attribution: d.attribution,
    maxZoom: d.maxZoom,
    id: d.id
  }).addTo(locator_map);

  let marker = this.L.marker([latitude, longitude], {
    icon: marker_icon
  }).addTo(locator_map);
};
