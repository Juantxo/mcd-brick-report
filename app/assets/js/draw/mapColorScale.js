this.MCD = window["MCD"] || {};
this.MCD["draw"] = window["MCD"]["draw"] || {};

this.MCD.draw.mapColorScale = d => {
  let data,
    width,
    height,
    axis,
    margin,
    svg,
    chart,
    bars,
    dot,
    triangleGroup,
    bar,
    x,
    scale,
    text,
    xdot,
    triangle,
    triangleSize,
    num,
    percentile,
    direction,
    k;

  data = d.d;
  direction = d.direction;
  k = d.bar;
  axis = d.axis;
  (margin = {
    top: d.margin.top,
    right: d.margin.right,
    bottom: d.margin.bottom,
    left: d.margin.left
  }),
    (width = d.width - margin.left - margin.right),
    (height = d.height - margin.top - margin.bottom);
  chart = this.d3.select("body").select(d.chartElement);
  // top single triangle on scale
  if (d.percentile) {
    percentile = +d.percentile;
  }

  if (direction === "vertical") {
    x = this.d3
      .scaleBand()
      .range([0, height])
      .padding(0);
    x.domain(
      data.map(d => {
        return d[axis];
      })
    );
  }

  if (direction === "horizontal") {
    x = this.d3
      .scaleBand()
      .range([0, width])
      .padding(0);
    x.domain(
      data.map(d => {
        return d[axis];
      })
    );
  }

  svg = chart
    .append("svg:svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .attr(
      "viewBox",
      "0 0 " +
        (width + margin.left + margin.right) +
        " " +
        (height + margin.top + margin.bottom)
    )
    .attr("preserveAspectRatio", "xMaxYMax meet");

  // top single triangle on scale
  if (percentile) {
    xdot = this.d3
      .scaleLinear()
      .domain([0, 100])
      .range([0, width]);

    // console.log('xdot(37)-->', xdot(37));

    num = +percentile;
    triangleSize = 12;
    triangle = this.d3
      .symbol()
      .type(this.d3.symbolTriangle)
      .size(triangleSize);

    dot = svg
      .append("svg:g")
      .attr("class", "dot")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    triangleGroup = dot
      .append("svg:g")
      .attr("class", "triangleGroup")
      .attr("transform", () => {
        return "translate(" + xdot(num) + "," + 0 + ")";
      });

    triangleGroup
      .append("path")
      .attr("d", triangle)
      .attr("stroke", "black")
      .attr("fill", "black")
      .attr("transform", () => {
        return "translate(" + 0 + "," + 10 + ")";
      });

    triangleGroup
      .append("text")
      .attr("x", () => {
        return 0;
      })
      .attr("y", () => {
        return 5;
      })
      .text(() => {
        return num;
      })
      .attr("font-size", "9px")
      .style("text-anchor", "middle")
      .style("font-weight", "900");

    margin.top += 15;
  }

  bars = svg
    .append("svg:g")
    .attr("class", "bars")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  bar = bars
    .selectAll("g")
    .data(data)
    .enter()
    .append("svg:g")
    .attr("transform", d => {
      if (direction === "horizontal") return "translate(" + x(d[axis]) + ",0)";
      if (direction === "vertical") return "translate(0," + x(d[axis]) + ")";
    });

  scale = bar
    .append("svg:rect")
    .attr("x", 0)
    .attr("y", 0)
    .attr("width", k.width)
    .attr("height", k.height)
    .attr("fill", d => {
      return d.color;
    });

  text = bar
    .append("svg:text")
    .attr("x", () => {
      return k.x;
    })
    .attr("y", () => {
      return k.y;
    })
    .text(d => {
      return d[axis];
    })
    .attr("font-size", "9px")
    .style("text-anchor", "start");
};
