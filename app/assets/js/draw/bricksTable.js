this.MCD = window["MCD"] || {};
this.MCD["draw"] = window["MCD"]["draw"] || {};

this.MCD.draw.bricksTable = d => {
  let pages,
    table,
    dataBaseBricks,
    total_of_bricks,
    bricks_per_page,
    total_of_pages,
    bricks_per_table,
    pages_counter,
    brick_counter,
    rows_counter,
    max_bricks;

  // sort bricks
  dataBaseBricks = d.d;
  dataBaseBricks.sort((a, b) => {
    return b.venta - a.venta;
  });

  // eliminate bricks
  max_bricks = 200;
  dataBaseBricks.length = max_bricks;

  // bricks

  total_of_bricks = dataBaseBricks.length;
  bricks_per_page = 80;
  bricks_per_table = bricks_per_page / 2;
  brick_counter = 0; // counting total data objects
  rows_counter = 0; // counting rows per table in a page

  // pages
  total_of_pages = Math.ceil(total_of_bricks / bricks_per_page);
  pages_counter = 0; // counting pages

  table = "";

  // loop for pages
  for (pages_counter; pages_counter < total_of_pages; pages_counter++) {
    table +=
      '<div data-size="A4">' +
      "<address>" +
      '<p class="address bkg-mcd">' +
      "<small>" +
      '<span class="template-name">{{name}}</span> | ' +
      '<span class="template-kind_building">{{kind_building}}</span> | ' +
      '<span class="template-day">{{day}}</span> | ' +
      '<span class="template-hour"> {{hour}}</span></small>' +
      "</p>" +
      "</address>";

    table +=
      "<article>" +
      "<section>" +
      '<div class="container">' +
      '<div class="row margin-top-32">' +
      '<div class="column tColumn six modules ">' +
      '<table class="tBricks">';

    for (rows_counter = 0; rows_counter < bricks_per_table; rows_counter++) {
      if (!!dataBaseBricks[brick_counter]) {
        if (rows_counter === 0) {
          table +=
            "<tr>" +
            "<th>Id Bricks</th>" +
            "<th>Municipio</th>" +
            "<th>Venta (€)</th>" +
            "</tr>";
        }
        table +=
          "<tr>" +
          "<td>" +
          dataBaseBricks[brick_counter].brick_id +
          "</td>" +
          "<td>" +
          dataBaseBricks[brick_counter].municipio +
          "</td>" +
          "<td>" +
          dataBaseBricks[brick_counter].venta +
          "</td>" +
          "</tr>";

        brick_counter++;
      }
    }

    table +=
      "</table>" +
      "</div>" +
      '<div class="column tGutter six modules"></div>' +
      '<div class="column tColumn six modules">' +
      '<table class="tBricks">';

    for (rows_counter = 0; rows_counter < bricks_per_table; rows_counter++) {
      if (!!dataBaseBricks[brick_counter]) {
        if (rows_counter === 0) {
          table +=
            "<tr>" +
            "<th>Id Bricks</th>" +
            "<th>Municipio</th>" +
            "<th>Venta (€)</th>" +
            "</tr>";
        }
        table +=
          "<tr>" +
          "<td>" +
          dataBaseBricks[brick_counter].brick_id +
          "</td>" +
          "<td>" +
          dataBaseBricks[brick_counter].municipio +
          "</td>" +
          "<td>" +
          dataBaseBricks[brick_counter].venta +
          "</td>" +
          "</tr>";

        brick_counter++;
      }
    }

    table += "</table>" + "</div>" + "</div>" + "</div>";

    table += " </section>" + "</article>";

    table +=
      "<footer>" +
      '<p class="page_number">' +
      (pages_counter + 2) +
      "</p>" +
      "</footer>";
    // end of table
    table += "</div>";
  }

  pages = document.getElementById(d.chartElement);
  pages.innerHTML += table;
};
