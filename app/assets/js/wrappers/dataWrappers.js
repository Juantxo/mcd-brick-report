this.MCD = window["MCD"] || {};
this.MCD["wrappers"] = window["MCD"]["wrappers"] || {};
this.MCD["wrappers"]["dataWrappers"] =
  window["MCD"]["wrappers"]["dataWrappers"] || {};

this.MCD.wrappers.dataWrappers = (() => {
  let getBricksTable = () => {
    return {
      d: this.MCD.wrappers.dataFilters.filterBricksTable(),
      chartElement: "bricksPages"
    };
  };

  let getbricksMap = () => {
    return {
      d: this.MCD.wrappers.dataFilters.filterbricksMap(),
      token: this.MCD.utils.getMapBoxToken()[0].value,
      id: "mapbox.streets",
      chartElement: "bricks_map",
      attribution: this.MCD.utils.getMapBoxAttribution(),
      zoomControl: false,
      zoomPosition: "topright",
      zoom: 13,
      maxZoom: 18,
      type: "Estudio",
      brand: "McDonalds",
      marker: "4772-marker-orange.svg",
      iconSize: [38, 95]
    };
  };

  let getBricksColorScale = () => {
    return {
      d: this.MCD.wrappers.dataFilters.filterBricksColorScale(),
      chartElement: "#bricks_scale", // el elemento de HTML seleccionado previamente en D3 donde se insertará el gráfico
      axis: "name",
      direction: "horizontal",
      width: 360,
      height: 42,
      margin: {
        top: 16,
        right: 0,
        bottom: 0,
        left: 0
      },
      bar: {
        height: 8,
        width: 45,
        x: 0,
        y: 24
      }
    };
  };

  return {
    getBricksTable: getBricksTable,
    getbricksMap: getbricksMap,
    getBricksColorScale: getBricksColorScale
  };
})();
