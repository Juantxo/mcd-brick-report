this.MCD = window["MCD"] || {};
this.MCD["wrappers"] = window["MCD"]["wrappers"] || {};
this.MCD["wrappers"]["jsonGetters"] =
  window["MCD"]["wrappers"]["jsonGetters"] || {};

this.MCD.wrappers.jsonGetters = (() => {
  let getAllAreas = d => {
    let tempAreas = [];
    let allAreas = [];
    let mappedAreas = [];

    tempAreas.push(d.properties.areas);
    tempAreas.push(d.properties.country);
    tempAreas.push(d.properties.province);
    tempAreas.push(d.properties.municipality);

    Object.keys(tempAreas[0]).forEach(function(key, v) {
      tempAreas[0][key]["id"] = key;
      allAreas.push(tempAreas[0][key]);
    });

    tempAreas[1]["id"] = "country";
    tempAreas[2]["id"] = "province";
    tempAreas[3]["id"] = "municipality";

    allAreas.push(tempAreas[1], tempAreas[2], tempAreas[3]);

    mappedAreas = allAreas.map(d => {
      return {
        id: d.id,
        values: d.properties.portals2017
      };
    });

    return mappedAreas;
  };

  // pass an Array of IDs [ "province", "minutes_foot_00_03", "minutes_car_00_05"]
  let getSomeAreas = arr => {
    let ids = arr;
    let idsL = arr.length;
    let i = 0;
    let result = [];

    for (i; i < idsL; i++) {
      let tempResult = this.MCD["areas"].filter(el => {
        return el.id == ids[i];
      });
      result.push(tempResult[0]);
    }
    return result;
  };

  // "car", "foot", "municipality","province", "country"
  let getTypeOfArea = string => {
    let result = [];

    result = this.MCD["areas"].filter(el => {
      return el.id.indexOf(string) >= 0;
    });

    return result;
  };

  // Array: Areas, string: Object key
  let getPortals2017 = (arr, string) => {
    let result = [];

    arr.forEach(function(d) {
      result.push({ id: d.id, values: d.values[string] });
    });

    return result;
  };

  let getDatosModelos = () => {
    return this.MCD.database.properties.datos_modelos;
  };

  let parseFormatMonth = d => {
    let timeParse = this.MCD.utils.locale_ES.timeParse;
    let timeFormat = this.MCD.utils.locale_ES.timeFormat;
    let parseYear = timeParse("%Y%m%d");
    let formatMonth = timeFormat("%B");

    d = d.map(el => {
      let o = Object.assign({}, el);
      o.month = parseYear(el["date"]);
      o.month = formatMonth(o.month);
      return o;
    });

    return d;
  };

  return {
    getAllAreas: getAllAreas,
    getSomeAreas: getSomeAreas,
    getTypeOfArea: getTypeOfArea,
    getPortals2017: getPortals2017,
    getDatosModelos: getDatosModelos,
    parseFormatMonth: parseFormatMonth
  };
})();
