this.MCD = window["MCD"] || {};
this.MCD["wrappers"] = window["MCD"]["wrappers"] || {};
this.MCD["wrappers"]["dataFilters"] =
  window["MCD"]["wrappers"]["dataFilters"] || {};

this.MCD.wrappers.dataFilters = (() => {
  let filterStudy = () => {
    let studio = this.MCD.database.properties.studio;
    let dayMonthYearFormat = this.MCD.utils.locale_ES.dayMonthYearFormat;
    let formatHour = this.MCD.utils.locale_ES.formatHour;
    let toTitleCase = this.MCD.utils.help.toTitleCase;

    return [
      {
        key: "id_study",
        value: studio.id_study,
        name: "Id",
        order: 10
      },
      {
        key: "name",
        value: studio.name,
        name: "Nombre",
        order: 20
      },
      {
        key: "state",
        value: toTitleCase(studio.state),
        name: "Estado",
        order: 30
      },
      {
        key: "day",
        value: dayMonthYearFormat(new Date(studio.timestamp)),
        name: "Fecha",
        order: 40
      },
      {
        key: "hour",
        value: formatHour(new Date(studio.timestamp)),
        name: "Hora",
        order: 50
      },

      {
        key: "province",
        value: studio.province,
        name: "Provincia",
        order: 140
      },

      {
        key: "kind_building",
        value: studio.kind_building,
        name: "Tipo de local",
        order: 200
      }
    ];
  };

  let filterBricksTable = () => {
    let bricks = this.MCD.database.properties.values;

    bricks.sort((a, b) => {
      return a.venta - b.venta;
    });

    return bricks;
  };

  let filterbricksMap = () => {
    let coordinates = this.MCD.database.geometry.coordinates;
    let bricksMap = {};

    bricksMap.x = coordinates[0]; // longitude
    bricksMap.y = coordinates[1]; // latitude
    bricksMap.name = this.MCD.database.properties.studio.name;
    return bricksMap;
  };
  let filterBricksColorScale = () => {
    let k = this.MCD.utils.colors.redSequentialColorScheme;

    return k;
  };

  return {
    filterStudy: filterStudy,
    filterBricksTable: filterBricksTable,
    filterbricksMap: filterbricksMap,
    filterBricksColorScale: filterBricksColorScale
  };
})();
