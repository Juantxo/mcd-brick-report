this.MCD["init_data"] = this.MCD["init_data"] || {};

this.MCD["init_data"] = () => {
  // ******************************************  //
  // ************** GET THE DATA ********************  //
  // ******************************************  //

  // PAGE 1 AND 2
  this.MCD.chartData.bricksTable = this.MCD.wrappers.dataWrappers.getBricksTable();

  // WE ARE HERE
  this.MCD.chartData.bricksMap = this.MCD.wrappers.dataWrappers.getbricksMap();
  this.MCD.chartData.bricksColorScale = this.MCD.wrappers.dataWrappers.getBricksColorScale();

  return false;
};
