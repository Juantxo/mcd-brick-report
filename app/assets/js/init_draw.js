this.MCD["init_draw"] = this.MCD["init_draw"] || {};

this.MCD["init_draw"] = () => {
  // ******************************************  //
  // ************** DRAW CHARTS ********************  //
  // ******************************************  //

  // PAG.1
  this.MCD.draw.bricksMap(this.MCD.chartData.bricksMap);

  this.MCD.draw.mapColorScale(this.MCD.chartData.bricksColorScale);
  // PAG. 2
  this.MCD.draw.bricksTable(this.MCD.chartData.bricksTable);

  // htmlTemplates
  this.MCD.draw.htmlTemplates();

  // table Study

  // Nümero de página
  //this.MCD.draw.pageNumber();

  return false;
};
