this.MCD = window["MCD"] || {};
this.MCD["utils"] = window["MCD"]["utils"] || {};

this.MCD.utils["help"] = window["MCD"]["utils"]["help"] || {};

this.MCD.utils.help = (w => {
  /**************toTitleCase**********/
  // DEMO
  /*
let demo = this.MCD.help.toTitleCase("demo");
*/

  let toTitleCase = str => {
    return str.replace(/\w\S*/g, function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  };

  let isNumber = n => {
    return !isNaN(parseFloat(n)) && isFinite(n);
  };

  /**
   * Merge properties of two Javascript objects (Population Pyramid)
   * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
   * @param obj1
   * @param obj2
   * @returns obj3 a new object based on obj1 and obj2
   */
  let merge_options = (obj1, obj2) => {
    let obj3 = {};
    for (let attrname in obj1) {
      obj3[attrname] = obj1[attrname];
    }
    for (let attrname in obj2) {
      obj3[attrname] = obj2[attrname];
    }
    return obj3;
  };

  let sum_object = obj => {
    var sum = 0;
    for (let el in obj) {
      if (obj.hasOwnProperty(el)) {
        sum += parseFloat(obj[el]);
      }
    }
    return sum;
  };

  let convertObjectToArray = obj => {
    let result = Object["entries"](obj).map(([k, v]) => ({ key: k, value: v }));
    return result;
  };
  return {
    toTitleCase: toTitleCase,
    isNumber: isNumber,
    merge_options: merge_options,
    sum_object: sum_object,
    convertObjectToArray: convertObjectToArray
  };
})(window);
