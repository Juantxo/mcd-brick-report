this.MCD = window["MCD"] || {};
this.MCD["utils"] = window["MCD"]["utils"] || {};
this.MCD.utils["locale_ES"] = window["MCD"].utils["locale_ES"] || {};

this.MCD.utils.locale_ES = (() => {
  const es_ES = {
    decimal: ",",
    thousands: ".",
    grouping: [3],
    currency: ["", "\u00a0€"],
    dateTime: "%a %b %e %X %Y",
    date: "%d/%m/%Y",
    time: "%H:%M:%S",
    periods: ["AM", "PM"],
    days: [
      "Domingo",
      "Lunes",
      "Martes",
      "Miércoles",
      "Jueves",
      "Viernes",
      "Sábado"
    ],
    shortDays: ["Dom", "Lun", "Mar", "Mi", "Jue", "Vie", "Sab"],
    months: [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
    ],
    shortMonths: [
      "Ene",
      "Feb",
      "Mar",
      "Abr",
      "May",
      "Jun",
      "Jul",
      "Ago",
      "Sep",
      "Oct",
      "Nov",
      "Dic"
    ]
  };

  let format_ES = this.d3.formatLocale(es_ES);
  let format = format_ES.format;
  let formatPrefix = format_ES.formatPrefix;

  let pSymbol = "%";

  let formatZero = format(",.0f"); // 7.5 --> 7 //
  let formatThousands = format(",.0f");
  let formatMillions = formatPrefix(".2", 1e6); // SI-prefix with two significant digits, "42,0M"
  let formatPercentage = format(".0%"); // 7 --> 700%
  let formatFixed = format(",.2f"); // 22.590,00
  let formatRoundTop = format(",.2r"); // -23.000
  let formatEuro = format("$.2f");
  let formatPercent = format(",.0f");
  let fo = format("+20");
  let fg = format(".2s");

  let time_ES = this.d3.timeFormatLocale(es_ES);
  let timeFormat = time_ES.format;
  let timeParse = time_ES.parse;
  let utcFormat = time_ES.utcFormat;
  let utcParse = time_ES.utcParse;

  let defaultDateFormat = timeFormat("%A %d, %H:%M"); // Lunes 23, 20:25
  let dayMonthYearFormat = timeFormat("%d/%m/%Y"); // 23/04/2012
  let dayMonthYearHourFormat = timeFormat("%d de %B de %Y, %H:%M"); // 23 de Abril de 2012, 20:25

  let formatLongYear = timeFormat("%Y"); // 2019
  let formatSortYear = timeFormat("%y"); // 2019

  let formatMonthLong = timeFormat("%B");
  let formatMonthSort = timeFormat("%b");

  let formatHour = timeFormat("%H:%M"); // 20:25

  return {
    es_ES: es_ES,
    format_ES: format_ES,
    timeFormat: timeFormat,
    timeParse: timeParse,
    formatZero: formatZero,
    formatMillions: formatMillions,
    formatThousands: formatThousands,
    formatPercentage: formatPercentage,
    formatPercent: formatPercent,
    formatFixed: formatFixed,
    formatRoundTop: formatRoundTop,
    formatEuro: formatEuro,
    fo: fo,
    fg: fg,
    defaultDateFormat: defaultDateFormat,
    dayMonthYearHourFormat: dayMonthYearHourFormat,
    dayMonthYearFormat: dayMonthYearFormat,
    formatLongYear: formatLongYear,
    formatSortYear: formatSortYear,
    formatMonthLong: formatMonthLong,
    formatMonthSort: formatMonthSort,
    formatHour: formatHour
  };
})();

//console.log(window["MCD"].utils.locale_ES.formatZero(25.33)); // => 25
//console.log(window["MCD"].utils.locale_ES.formatMillions(225000999)); // => 225,00M
//console.log(window["MCD"].utils.locale_ES.formatPercentage(22.36)); // => 2236%
//console.log(window["MCD"].utils.locale_ES.formatThousands(22589)); // => 222.589
//console.log(window["MCD"].utils.locale_ES.formatFixed(22589.999988)); // => 22.590,00
//console.log(window["MCD"].utils.locale_ES.formatRoundTop(-2.3669)); // => -2,4
//console.log(window["MCD"].utils.locale_ES.fo(2.3669)); // =>  +2,3669
//console.log(window["MCD"].utils.locale_ES.fg(42e6)); // => 42M

//console.log(window["MCD"].utils.locale_ES.defaultDateFormat(new Date())); // => Viernes 13, 11:31
//console.log(window["MCD"].utils.locale_ES.formatLongYear(new Date())); // => 2019
//console.log(window["MCD"].utils.locale_ES.formatSortYear(new Date())); // => 19
//console.log(window["MCD"].utils.locale_ES.formatMonthLong(new Date())); // => Septiembre
//console.log(window["MCD"].utils.locale_ES.formatMonthSort(new Date())); // => Sep

console.log(
  window["MCD"].utils.locale_ES.defaultDateFormat(
    new Date("2012-04-23T18:25:43.511Z")
  )
); // => Lunes 23, 20:25

console.log(
  window["MCD"].utils.locale_ES.dayMonthYearFormat(
    new Date("2012-04-23T18:25:43.511Z")
  )
); // => 23/04/2012

console.log(
  window["MCD"].utils.locale_ES.dayMonthYearHourFormat(
    new Date("2012-04-23T18:25:43.511Z")
  )
); // => 23 de Abril de 2012, 20:25
