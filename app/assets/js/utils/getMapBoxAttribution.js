this.MCD = window["MCD"] || {};
this.MCD["utils"] = window["MCD"]["utils"] || {};
this.MCD["utils"]["getMapBoxAttribution"] =
  window["MCD"]["utils"]["getMapBoxAttribution"] || {};

this.MCD.utils.getMapBoxAttribution = () => {
  return (
    'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="http://mapbox.com">Mapbox</a>'
  );
};
