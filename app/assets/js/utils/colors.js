this.MCD = window["MCD"] || {};
this.MCD["utils"] = window["MCD"]["utils"] || {};
this.MCD.utils["colors"] = window["MCD"]["utils"]["colors"] || {};

this.MCD.utils.colors.vColors = {
  white: "#ffffff",
  black: "#000000",
  green1: "#1b9740",
  green2: "#1FAD4A",
  green3: "#9BCC3B",
  green4: "#D6E029",
  morado1: "#D73888",
  morado2: "#cc66cc",
  morado4: "#7f1b55",
  brown: "#c1baa9",
  azul1: "#3182bd",
  azul2: "#9c9ede",
  azul4: "#1a316b",
  coral: "#FF7F50",
  tomato: "#FF6347",
  rojo: "#e6550d",
  amarillo: "#FFD700",
  naranja: "#FFA500",
  gris1: "#D3D3D3",
  gris2: "#eeeeee",
  whiteSmoke: "#f5f5f5",
  teal: "#46a4b4",
  red_s1: "#fff5f0",
  red_s2: "#fee0d2",
  red_s3: "#fcbba1",
  red_s4: "#fc9272",
  red_s5: "#fb6a4a",
  red_s6: "#ef3b2c",
  red_s7: "#cb181d",
  red_s8: "#99000d"
};

this.MCD.utils.colors.greenToRedColorScheme = [
  {
    color: this.MCD.utils.colors.vColors.green1,
    value: 37,
    name: "37k",
    intensity: "My bajo"
  },
  {
    color: this.MCD.utils.colors.vColors.green2,
    value: 52,
    name: "37-52k",
    intensity: " "
  },
  {
    color: this.MCD.utils.colors.vColors.green3,
    value: 61,
    name: "52-61k",
    intensity: " "
  },
  {
    color: this.MCD.utils.colors.vColors.green4,
    value: 68,
    name: "61-68k",
    intensity: " "
  },
  {
    color: this.MCD.utils.colors.vColors.amarillo,
    value: 79,
    name: "68-79k",
    intensity: " "
  },
  {
    color: this.MCD.utils.colors.vColors.naranja,
    value: 102,
    name: "79-102k",
    intensity: " "
  },
  {
    color: this.MCD.utils.colors.vColors.tomato,
    value: 120,
    name: "102-120k",
    intensity: " "
  },
  {
    color: this.MCD.utils.colors.vColors.rojo,
    value: 142,
    name: "120-142k",
    intensity: "Muy alto"
  }
];
this.MCD.utils.colors.redSequentialColorScheme = [
  {
    color: this.MCD.utils.colors.vColors.red_s1,
    value: 37,
    name: "37k",
    intensity: "My bajo"
  },
  {
    color: this.MCD.utils.colors.vColors.red_s2,
    value: 52,
    name: "37-52k",
    intensity: " "
  },
  {
    color: this.MCD.utils.colors.vColors.red_s3,
    value: 61,
    name: "52-61k",
    intensity: " "
  },
  {
    color: this.MCD.utils.colors.vColors.red_s4,
    value: 68,
    name: "61-68k",
    intensity: " "
  },
  {
    color: this.MCD.utils.colors.vColors.red_s5,
    value: 79,
    name: "68-79k",
    intensity: " "
  },
  {
    color: this.MCD.utils.colors.vColors.red_s6,
    value: 102,
    name: "79-102k",
    intensity: " "
  },
  {
    color: this.MCD.utils.colors.vColors.red_s7,
    value: 120,
    name: "102-120k",
    intensity: " "
  },
  {
    color: this.MCD.utils.colors.vColors.red_s8,
    value: 142,
    name: "120-142k",
    intensity: "Muy alto"
  }
];
