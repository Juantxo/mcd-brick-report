this.MCD = window["MCD"] || {};
this.MCD["utils"] = window["MCD"]["utils"] || {};
this.MCD["utils"]["getMapBoxToken"] =
  window["MCD"]["utils"]["getMapBoxToken"] || {};

this.MCD.utils.getMapBoxToken = () => {
  let mapBoxUrl =
    "https://api.mapbox.com/styles/v1/abacus-consulting/cjxem7nyt09cj1cmkiz7v371c/tiles/256/{z}/{x}/{y}?access_token=";
  return [
    {
      key: "main",
      value:
        mapBoxUrl +
        "pk.eyJ1IjoiYWJhY3VzLWNvbnN1bHRpbmciLCJhIjoiY2p4ZW00Ync1MGN4ejN6czAybGF3cXowYiJ9.Gy0d0IYnSxyJCjuPp8GRUw"
    }
  ];
};

this.MCD.utils.getMapBoxAttribution = () => {
  return (
    'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="http://mapbox.com">Mapbox</a>'
  );
};
