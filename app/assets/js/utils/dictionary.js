this.MCD = window["MCD"] || {};
this.MCD["utils"] = window["MCD"]["utils"] || {};
this.MCD.utils["dictionary"] = window["MCD"]["utils"]["dictionary"] || {};

// **********************************************************************************
// DICCIONARIOS
// **********************************************************************************

this.MCD.utils.dictionary["continents"] = [
  {
    name: "v_africa",
    long_title: "África",
    short_title: "África",
    word_title: "África"
  },
  {
    name: "v_america",
    long_title: "América",
    short_title: "América",
    word_title: "América"
  },
  {
    name: "v_asia",
    long_title: "Asia",
    short_title: "Asia",
    word_title: "Asia"
  },
  {
    name: "v_euro",
    long_title: "Europa",
    short_title: "Europa",
    word_title: "Europa"
  },
  {
    name: "v_resto",
    long_title: "Oceanía",
    short_title: "Oceanía",
    word_title: "Oceanía"
  },
  {
    name: "v_ue27",
    long_title: "UE27",
    short_title: "UE27",
    word_title: "UE27"
  }
];

this.MCD.utils.dictionary["countries"] = [
  {
    name: "v_belg",
    long_title: "Bélgica ",
    short_title: "Bélgica ",
    word_title: "Bélgica "
  },
  {
    name: "v_dina",
    long_title: "Dinamarca ",
    short_title: "Dinamarca ",
    word_title: "Dinamarca "
  },
  {
    name: "v_alem",
    long_title: "Alemania ",
    short_title: "Alemania ",
    word_title: "Alemania "
  },
  {
    name: "v_grec",
    long_title: "Grecia ",
    short_title: "Grecia ",
    word_title: "Grecia "
  },
  {
    name: "v_fran",
    long_title: "Francia ",
    short_title: "Francia ",
    word_title: "Francia "
  },
  {
    name: "v_irla",
    long_title: "Irlanda ",
    short_title: "Irlanda ",
    word_title: "Irlanda "
  },
  {
    name: "v_ital",
    long_title: "Italia ",
    short_title: "Italia ",
    word_title: "Italia "
  },
  {
    name: "v_luxe",
    long_title: "Luxemburgo ",
    short_title: "Luxemburgo ",
    word_title: "Lux."
  },
  {
    name: "v_hola",
    long_title: "Holanda ",
    short_title: "Holanda ",
    word_title: "Holanda "
  },
  {
    name: "v_port",
    long_title: "Portugal ",
    short_title: "Portugal ",
    word_title: "Portugal "
  },
  {
    name: "v_reiu",
    long_title: "Reino Unido ",
    short_title: "Reino Unido ",
    word_title: "R. Unido "
  },
  {
    name: "v_aust",
    long_title: "Austria ",
    short_title: "Austria ",
    word_title: "Austria "
  },
  {
    name: "v_finl",
    long_title: "Finlandia ",
    short_title: "Finlandia ",
    word_title: "Finlandia "
  },
  {
    name: "v_suec",
    long_title: "Suecia ",
    short_title: "Suecia ",
    word_title: "Suecia "
  },
  {
    name: "v_chip",
    long_title: "Chipre ",
    short_title: "Chipre ",
    word_title: "Chipre "
  },
  {
    name: "v_eslo",
    long_title: "Eslovenia ",
    short_title: "Eslovenia ",
    word_title: "Eslovenia "
  },
  {
    name: "v_esto",
    long_title: "Estonia ",
    short_title: "Estonia ",
    word_title: "Estonia "
  },
  {
    name: "v_hung",
    long_title: "Hungría ",
    short_title: "Hungría ",
    word_title: "Hungría "
  },
  {
    name: "v_leto",
    long_title: "Letonia ",
    short_title: "Letonia ",
    word_title: "Letonia "
  },
  {
    name: "v_litu",
    long_title: "Lituania ",
    short_title: "Lituania ",
    word_title: "Lituania "
  },
  {
    name: "v_malt",
    long_title: "Malta ",
    short_title: "Malta ",
    word_title: "Malta "
  },
  {
    name: "v_polo",
    long_title: "Polonia ",
    short_title: "Polonia ",
    word_title: "Polonia "
  },
  {
    name: "v_rche",
    long_title: "Rep. Checa ",
    short_title: "Rep. Checa ",
    word_title: "R. Checa "
  },
  {
    name: "v_resl",
    long_title: "Rep. Eslovaca ",
    short_title: "Rep. Eslovaca ",
    word_title: "R. Eslovaca "
  },
  {
    name: "v_bulg",
    long_title: "Bulgaria ",
    short_title: "Bulgaria ",
    word_title: "Bulgaria "
  },
  {
    name: "v_ruma",
    long_title: "Rumania ",
    short_title: "Rumania ",
    word_title: "Rumania "
  },
  {
    name: "v_noru_",
    long_title: "Noruega",
    short_title: "Noruega",
    word_title: "Noruega"
  },
  {
    name: "v_rusia",
    long_title: "Rusia ",
    short_title: "Rusia ",
    word_title: "Rusia "
  },
  {
    name: "v_ucra",
    long_title: "Ucrania ",
    short_title: "Ucrania ",
    word_title: "Ucrania "
  },
  {
    name: "v_argel",
    long_title: "Argelia ",
    short_title: "Argelia ",
    word_title: "Argelia "
  },
  {
    name: "v_marr",
    long_title: "Marruecos ",
    short_title: "Marruecos ",
    word_title: "Marruecos "
  },
  {
    name: "v_niger",
    long_title: "Nigeria ",
    short_title: "Nigeria ",
    word_title: "Nigeria "
  },
  {
    name: "v_seneg",
    long_title: "Senegal ",
    short_title: "Senegal ",
    word_title: "Senegal "
  },
  {
    name: "v_arge",
    long_title: "Argentina ",
    short_title: "Argentina ",
    word_title: "Argentina "
  },
  {
    name: "v_boli",
    long_title: "Bolivia ",
    short_title: "Bolivia ",
    word_title: "Bolivia "
  },
  {
    name: "v_bras",
    long_title: "Brasil ",
    short_title: "Brasil ",
    word_title: "Brasil "
  },
  {
    name: "v_colo",
    long_title: "Colombia ",
    short_title: "Colombia ",
    word_title: "Colombia "
  },
  {
    name: "v_cuba",
    long_title: "Cuba ",
    short_title: "Cuba ",
    word_title: "Cuba "
  },
  {
    name: "v_chile",
    long_title: "Chile ",
    short_title: "Chile ",
    word_title: "Chile "
  },
  {
    name: "v_ecua",
    long_title: "Ecuador ",
    short_title: "Ecuador ",
    word_title: "Ecuador "
  },
  {
    name: "v_parag",
    long_title: "Paraguay ",
    short_title: "Paraguay ",
    word_title: "Paraguay "
  },
  {
    name: "v_peru",
    long_title: "Perú ",
    short_title: "Perú ",
    word_title: "Perú "
  },
  {
    name: "v_rdom",
    long_title: "R.Dominicana ",
    short_title: "R.Dominicana ",
    word_title: "R. Dom."
  },
  {
    name: "v_urug",
    long_title: "Uruguay ",
    short_title: "Uruguay ",
    word_title: "Uruguay "
  },
  {
    name: "v_venez",
    long_title: "Venezuela ",
    short_title: "Venezuela ",
    word_title: "Venezuela "
  },
  {
    name: "v_chin",
    long_title: "China ",
    short_title: "China ",
    word_title: "China "
  },
  {
    name: "v_pakis",
    long_title: "Pakistan ",
    short_title: "Pakistán ",
    word_title: "Pakistán "
  }
];

this.MCD.utils.dictionary.wording = [
  {
    name: "minutes_car_00_03",
    long_title: "3min. coche",
    short_title: "3min. coche",
    word_title: "3min."
  },
  {
    name: "minutes_car_00_05",
    long_title: "5min. coche",
    short_title: "5min. coche",
    word_title: "5min."
  },
  {
    name: "minutes_car_00_10",
    long_title: "10min. coche",
    short_title: "10min. coche",
    word_title: "10min."
  },
  {
    name: "minutes_car_00_15",
    long_title: "15min. coche",
    short_title: "15min. coche",
    word_title: "15min."
  },

  {
    name: "minutes_foot_00_03",
    long_title: "3min. andando",
    short_title: "3min. andando",
    word_title: "3min."
  },
  {
    name: "minutes_foot_00_05",
    long_title: "5min. andando",
    short_title: "5min. andando",
    word_title: "5min."
  },
  {
    name: "minutes_foot_00_10",
    long_title: "10min. andando",
    short_title: "10min. andando",
    word_title: "10min."
  },
  {
    name: "minutes_foot_00_15",
    long_title: "15min. andando",
    short_title: "15min. andando",
    word_title: "15min."
  },
  {
    name: "country",
    long_title: "España",
    short_title: "España",
    word_title: "Esp."
  },

  {
    name: "province",
    long_title: "Provincia",
    short_title: "Provincia",
    word_title: "Prov."
  },
  {
    name: "municipality",
    long_title: "Municipio",
    short_title: "Municipio",
    word_title: "Mun."
  },

  {
    name: "pob_0_14",
    long_title: "0-14 años",
    short_title: "0-14",
    word_title: "0-14"
  },
  {
    name: "pob_15_64",
    long_title: "15-64 años",
    short_title: "15-65",
    word_title: "15-65"
  },
  {
    name: "pob_mas64",
    long_title: "Más de 64 años",
    short_title: "+64",
    word_title: "+64"
  },
  {
    name: "pob_mas64",
    long_title: "Más de 64 años",
    short_title: "+64",
    word_title: "+64"
  },
  {
    name: "tot_espa",
    long_title: "Población española",
    short_title: "Española",
    word_title: "Española"
  },

  {
    name: "tot_extra",
    long_title: "Población extranjera",
    short_title: "Extranjera",
    word_title: "Extranjera"
  },
  {
    name: "enCoche",
    long_title: "En coche",
    short_title: "En coche",
    word_title: "Coche"
  },

  {
    name: "v_comercial",
    long_title: "Suelo comercial",
    short_title: "Comercial",
    word_title: "Comercial"
  },

  {
    name: "v_deportivo",
    long_title: "Suelo deportivo",
    short_title: "Deportivo",
    word_title: "Deportivo"
  },

  {
    name: "v_industrial",
    long_title: "Suelo industrial",
    short_title: "Industrial",
    word_title: "Industrial"
  },

  {
    name: "v_ocio_hostel",
    long_title: "Suelo ocio",
    short_title: "Ocio",
    word_title: "Ocio"
  },

  {
    name: "v_oficinas",
    long_title: "Suelo oficinas",
    short_title: "Oficinas",
    word_title: "Oficinas"
  },
  {
    name: "v_residencial",
    long_title: "Suelo residencial",
    short_title: "Residencial",
    word_title: "Residencial"
  },
  {
    name: "v_sanidad_ben",
    long_title: "Suelo sanidad",
    short_title: "Sanidad",
    word_title: "Sanidad"
  },

  {
    name: "vivcott",
    long_title: "Colectiva",
    short_title: "Colectiva",
    word_title: "Colectiva"
  },
  {
    name: "vivfpt",
    long_title: "Principal",
    short_title: "Principal",
    word_title: "Principal"
  },
  {
    name: "vivfnpot",
    long_title: "Otra",
    short_title: "Otra",
    word_title: "Otra"
  },

  {
    name: "vivfnps",
    long_title: "Secundaria",
    short_title: "Secundaria",
    word_title: "Secundaria"
  },

  {
    name: "vivfnpv",
    long_title: "Vacía",
    short_title: "Vacía",
    word_title: "Vacía"
  },
  {
    name: "v_alq",
    long_title: "Alquiler",
    short_title: "Alquiler",
    word_title: "Alquiler"
  },

  {
    name: "v_cedgra",
    long_title: "Cedida",
    short_title: "Cedida",
    word_title: "Cedida"
  },
  {
    name: "v_pphod",
    long_title: "Herencia",
    short_title: "Herencia",
    word_title: "Herencia"
  },
  {
    name: "v_oform",
    long_title: "Otra",
    short_title: "Otra",
    word_title: "Otra"
  },

  {
    name: "v_ppctp",
    long_title: "Pagada",
    short_title: "Pagada",
    word_title: "Pagada"
  },

  {
    name: "v_ppcpp",
    long_title: "Pago pendiente",
    short_title: "Pago pendiente",
    word_title: "Pago pend."
  },

  {
    name: "porc_bares_restaurantes",
    long_title: "Bares y restaurantes",
    short_title: "Bares y rest.",
    word_title: "Hostelería"
  },
  {
    name: "porc_bancos",
    long_title: "Bancos",
    short_title: "Bancos",
    word_title: "Banca"
  },
  {
    name: "porc_salud_belleza",
    long_title: "Salud y belleza",
    short_title: "Salud y belleza",
    word_title: "Salud"
  },

  {
    name: "porc_alimentacion",
    long_title: "Alimentación",
    short_title: "Alimentación",
    word_title: "Alimentación"
  },
  {
    name: "porc_moda",
    long_title: "Moda",
    short_title: "Moda",
    word_title: "Moda"
  },
  {
    name: "mcd",
    long_title: "McDonald's",
    short_title: "McDonald's",
    word_title: "McDonald's"
  },
  {
    name: "resto",
    long_title: "Resto",
    short_title: "Resto",
    word_title: "Resto"
  }

  // Añadir aquí conforme se vayan necesitando
];

/**************getRealName**********/
// dictionary getter
// nommbre de la zona, corto, medio, largo
// se puede pasar un objeto o un string
// diccionary key: el nombre del diccionario STRING
// n: la columna del diccionario que devuelve STRING
// z: valor que se pasa (objeto o string) STRING / OBJECT
// k: en caso de objeto, la key del objeto que se quiere comprobar STRING

// DEMO
/*
let i = this.MCD.utils.dictionary.getRealName(
  this.MCD.utils.dictionary.wording,
  "word_title",
  "3min andando",
  null
);
console.log("getRealName", i);
*/

this.MCD.utils.dictionary.getRealName = (dic, n, z, k) => {
  let i = 0;
  let name;
  let dicL = dic.length;

  for (i; i < dicL; i++) {
    if (z && typeof z === "object" && z.constructor === Object) {
      if (dic[i]["name"] === z[k]) {
        name = dic[i][n];
      }
    }

    if (typeof z === "string" || z instanceof String) {
      if (dic[i]["name"] === z) {
        name = dic[i][n];
      }
    }
  }
  if (!name) {
    name = z[k] ? z[k] : z;
  }

  return name;
};

// **********************************************************************************
// nombres de los rangos de edad
this.MCD.utils.dictionary.getTheRangeName = l => {
  let k, name, tv, tempOne, tw, ty, tyy, tyz;
  k = l.trim();

  if (k.indexOf("v_p") >= 0) {
    name = k.replace("v_p", "");
    name = name.replace("_", "");
  } else if (k.indexOf("v_men") >= 0) {
    name = k.replace("v_men", "");
    name = name.replace("_", "");
  } else if (k.indexOf("v_women") >= 0) {
    name = k.replace("v_women", "");
    name = name.replace("_", "");
  } else if (k.indexOf("average_es_men") >= 0) {
    name = k.replace("average_es_men", "");
    name = name.replace("_", "");
  } else if (k.indexOf("average_es_women") >= 0) {
    name = k.replace("average_es_women", "");
    name = name.replace("_", "");
  } else {
    name = "0000";
  }

  // name = k.replace('pob', '');
  tv = name.substring(0, 2) + "-" + name.substring(2);
  tempOne = tv.substring(0, 1);
  if (tempOne == "0") {
    tw = tv.replace("00", "0");
    ty = tw.replace("04", "4");
    tyy = ty.replace("05", "5");
    tyz = tyy.replace("09", "9");
  } else {
    if (tv === "85m" || tv === "85-m") {
      tv = "+85";
      return tv;
    } else {
      return tv;
    }
  }
  return tyz;
};
